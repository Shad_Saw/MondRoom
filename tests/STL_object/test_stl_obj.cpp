#include <cmath>
#include "STL/stl_object/ObjectStl.hpp"
#include "STL/Parser/Parser.hpp"
#include "STL/Parser/ParserAscii.hpp"
#include "STL/Parser/ParserBinary.hpp"
#include "gtest/gtest.h"



class obj_stl_test : public ::testing::Test
{
    static const inline std::string path_deco = "../../bank_stl/Decorations/";
    std::shared_ptr<ObjectStl> original = stl::Parser::parse(
            path_deco + "tetra_ascii.stl");

protected:
    ObjectStl obj{*original};

    void SetUp() override
    {
        obj = *original;
    }
};

TEST_F(obj_stl_test, init)
{
    std::string res = "solid tetraedre\n"
                      "    facet normal 0 0 -1\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 10 0 0\n"
                      "        vertex 0 10 0\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal -1 0 0\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 0 10 0\n"
                      "        vertex 0 0 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal 0 -1 0\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 10 0 0\n"
                      "        vertex 0 0 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal 0.57735 0.57735 0.57735\n"
                      "        outer loop\n"
                      "        vertex 0 10 0\n"
                      "        vertex 10 0 0\n"
                      "        vertex 0 0 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "endsolid tetraedre";
    std::stringstream buffer;
    buffer << obj;
    EXPECT_EQ(buffer.str(), res);
}

TEST_F(obj_stl_test, translate)
{
    std::string res = "solid tetraedre\n"
                      "    facet normal 0 0 -1\n"
                      "        outer loop\n"
                      "        vertex 10 5 0\n"
                      "        vertex 20 5 0\n"
                      "        vertex 10 15 0\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal -1 0 0\n"
                      "        outer loop\n"
                      "        vertex 10 5 0\n"
                      "        vertex 10 15 0\n"
                      "        vertex 10 5 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal 0 -1 0\n"
                      "        outer loop\n"
                      "        vertex 10 5 0\n"
                      "        vertex 20 5 0\n"
                      "        vertex 10 5 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal 0.57735 0.57735 0.57735\n"
                      "        outer loop\n"
                      "        vertex 10 15 0\n"
                      "        vertex 20 5 0\n"
                      "        vertex 10 5 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "endsolid tetraedre";

    std::stringstream buffer;
    Transform t1 = Transform::Identity();
    t1.translate(Vect3{10, 5, 0});
    obj.transform(t1);
    buffer << obj;

    EXPECT_EQ(buffer.str(), res);
}

TEST_F(obj_stl_test, rotate)
{
    std::string res =
            "solid tetraedre\n"
            "    facet normal 0 -0 -1\n"
            "        outer loop\n"
            "        vertex 0 0 0\n"
            "        vertex -10 -8.74228e-07 0\n"
            "        vertex 8.74228e-07 -10 0\n"
            "        endloop\n"
            "    endfacet\n"
            "\n"
            "    facet normal 1 8.74228e-08 0\n"
            "        outer loop\n"
            "        vertex 0 0 0\n"
            "        vertex 8.74228e-07 -10 0\n"
            "        vertex 0 0 10\n"
            "        endloop\n"
            "    endfacet\n"
            "\n"
            "    facet normal -8.74228e-08 1 0\n"
            "        outer loop\n"
            "        vertex 0 0 0\n"
            "        vertex -10 -8.74228e-07 0\n"
            "        vertex 0 0 10\n"
            "        endloop\n"
            "    endfacet\n"
            "\n"
            "    facet normal -0.57735 -0.57735 0.57735\n"
            "        outer loop\n"
            "        vertex 8.74228e-07 -10 0\n"
            "        vertex -10 -8.74228e-07 0\n"
            "        vertex 0 0 10\n"
            "        endloop\n"
            "    endfacet\n"
            "\n"
            "endsolid tetraedre";
    std::stringstream buffer;
    Transform r = Transform::Identity();
    r.rotate(AngleAxis(M_PI, Vect3{0, 0, 1}));
    obj.transform(r);
    buffer << obj;

    EXPECT_EQ(buffer.str(), res);
}

TEST_F(obj_stl_test, scale)
{
    std::string res = "solid tetraedre\n"
                      "    facet normal 0 0 -1\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 20 0 0\n"
                      "        vertex 0 20 0\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal -1 0 0\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 0 20 0\n"
                      "        vertex 0 0 20\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal 0 -1 0\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 20 0 0\n"
                      "        vertex 0 0 20\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal 0.57735 0.57735 0.57735\n"
                      "        outer loop\n"
                      "        vertex 0 20 0\n"
                      "        vertex 20 0 0\n"
                      "        vertex 0 0 20\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "endsolid tetraedre";

    std::stringstream buffer;
    Transform r = Transform::Identity();
    r.scale(2);
    obj.transform(r);
    buffer << obj;

    EXPECT_EQ(buffer.str(), res);
}

TEST_F(obj_stl_test, points)
{
    std::vector<Point3> res =
            {
                    // t1
                    Point3{0, 0, 0},
                    Point3{10, 0, 0},
                    Point3{0, 10, 0},
                    // t2
                    Point3{0, 0, 0},
                    Point3{0, 10, 0},
                    Point3{0, 0, 10},
                    // t3
                    Point3{0, 0, 0},
                    Point3{10, 0, 0},
                    Point3{0, 0, 10},
                    // t4
                    Point3{0, 10, 0},
                    Point3{10, 0, 0},
                    Point3{0, 0, 10},
            };
    EXPECT_EQ(obj.get_points(), res);
}


TEST_F(obj_stl_test, to_file_ascii)
{
    std::string tmp_file = "/tmp/tmp_stl_obj.stl";
    obj.to_file(tmp_file, true);
    stl::ParserAscii parser;
    auto obj_final = parser(tmp_file);
    std::stringstream buf_origin;
    std::stringstream buf_final;

    buf_origin << obj;
    buf_final << *obj_final;

    EXPECT_EQ(buf_origin.str(), buf_final.str());
}

TEST_F(obj_stl_test, to_file_binary)
{
    std::string tmp_file = "/tmp/tmp_stl_obj.stl";
    obj.to_file(tmp_file, false);
    stl::ParserBinary parser;
    auto obj_final = parser(tmp_file);
    std::stringstream buf_origin;
    std::stringstream buf_final;

    buf_origin << obj;
    buf_final << *obj_final;

    EXPECT_EQ(buf_origin.str(), buf_final.str());
}
