#include <array>
#include <cmath>
#include "STL/stl_object/Triangle.hpp"
#include "gtest/gtest.h"

class triangle_test : public ::testing::Test
{
protected:
    Triangle t;

    triangle_test()
            : t{Vect3Normal(), std::array<Point3, 3>{}}
    {}

    void SetUp() override
    {
        Point3 p1{0, 20, 0};
        Point3 p2{0, 0, 0};
        Point3 p3{0, 0, 20};
        Vect3Normal n{-1, 0, 0};
        t = Triangle{n, p1, p2, p3};
    }
};

TEST_F(triangle_test, init)
{
    std::string res =
            "    facet normal -1 0 0\n"
            "        outer loop\n"
            "        vertex 0 20 0\n"
            "        vertex 0 0 0\n"
            "        vertex 0 0 20\n"
            "        endloop\n"
            "    endfacet";
    std::stringstream buffer;
    buffer << t;
    EXPECT_EQ(buffer.str(), res);
}

TEST_F(triangle_test, translate)
{
    std::string res =
            "    facet normal -1 0 0\n"
            "        outer loop\n"
            "        vertex 10 25 0\n"
            "        vertex 10 5 0\n"
            "        vertex 10 5 20\n"
            "        endloop\n"
            "    endfacet";

    std::stringstream buffer;
    Transform t1 = Transform::Identity(); // a ne pas oublier Identity!
    t1.translate(Vect3{10, 5, 0});
    t.transform(t1);
    buffer << t;

    EXPECT_EQ(buffer.str(), res);
}

TEST_F(triangle_test, rotate)
{
    std::string res =
            "    facet normal 1 8.74228e-08 0\n"
            "        outer loop\n"
            "        vertex 1.74846e-06 -20 0\n"
            "        vertex 0 0 0\n"
            "        vertex 0 0 20\n"
            "        endloop\n"
            "    endfacet";
    std::stringstream buffer;
    Transform r = Transform::Identity(); // a ne pas oublier Identity!
    r.rotate(AngleAxis(M_PI, Vect3{0, 0, 1}));
    t.transform(r);
    buffer << t;

    EXPECT_EQ(buffer.str(), res);
}

TEST_F(triangle_test, scale)
{
    std::string res =
            "    facet normal -1 0 0\n"
            "        outer loop\n"
            "        vertex 0 40 0\n"
            "        vertex 0 0 0\n"
            "        vertex 0 0 40\n"
            "        endloop\n"
            "    endfacet";
    std::stringstream buffer;
    Transform r = Transform::Identity(); // a ne pas oublier Identity!
    r.scale(2);
    t.transform(r);
    buffer << t;

    EXPECT_EQ(buffer.str(), res);
}
