#include <array>
#include <cmath>
#include "STL/stl_object/Vect3Normal.hpp"
#include "gtest/gtest.h"


static const constexpr float EPSILON = 10E-6;

class normal_test : public ::testing::Test
{
protected:
    Vect3Normal n0;
    Vect3Normal n1;
    Vect3Normal n2;
    void SetUp() override
    {
        n0 = Vect3Normal();
        n1 = Vect3Normal{1, 0, 1};
        n2 = Vect3Normal{std::array<float, 3>{0, -3, 6}};
    }
};

TEST_F(normal_test, init)
{
    // p0
    EXPECT_EQ(n0.x(), 0);
    EXPECT_EQ(n0.y(), 0);
    EXPECT_EQ(n0.z(), 0);
    // P1
    EXPECT_EQ(n1.x(), n1.z());
    EXPECT_EQ(n1.y(), 0);
    // P2
    EXPECT_EQ(n2.x(), 0);
    EXPECT_EQ(n2.y(), n2.y());
    EXPECT_EQ(n2.z(), n2.z());

}

TEST_F(normal_test, translate)
{
    Transform t1 = Transform::Identity(); // a ne pas oublier Identity!
    t1.translate(Vect3{1, 0, 0});
    n0.transform(t1);
    EXPECT_EQ(n0.x(), 0);
    EXPECT_EQ(n0.y(), 0);
    EXPECT_EQ(n0.z(), 0);

    n1.transform(t1);
    EXPECT_EQ(n1.x(), n1.z());
    EXPECT_EQ(n1.y(), 0);

    Transform t2 = Transform::Identity();
    t2.translate(Vect3{1, 2, -4});
    n2.transform(t2);
    EXPECT_EQ(n2.x(), 0);
    EXPECT_EQ(n2.y(), n2.y());
    EXPECT_EQ(n2.z(), n2.z());
}

TEST_F(normal_test, rotate)
{
    Transform r = Transform::Identity(); // a ne pas oublier Identity!
    r.rotate(AngleAxis(M_PI, Vect3{0, 0, 1}));
    n0.transform(r);
    EXPECT_EQ(n0.x(), 0);
    EXPECT_EQ(n0.y(), 0);
    EXPECT_EQ(n0.z(), 0);

    Vect3Normal n2_copy{n2};
    n2.transform(r);
    ASSERT_NEAR(n2.x(), -n2_copy.x(), EPSILON);
    ASSERT_NEAR(n2.y(), -n2_copy.y(), EPSILON);
    ASSERT_NEAR(n2.z(), n2_copy.z(), EPSILON);
}

TEST_F(normal_test, scale)
{
    Transform r = Transform::Identity(); // a ne pas oublier Identity!
    r.scale(42);
    n0.transform(r);
    EXPECT_EQ(n0.x(), 0);
    EXPECT_EQ(n0.y(), 0);
    EXPECT_EQ(n0.z(), 0);

    n1.transform(r);
    auto n1_copy{n1};
    EXPECT_EQ(n1.x(), n1_copy.x());
    EXPECT_EQ(n1.y(), n1_copy.y());
    EXPECT_EQ(n1.z(), n1_copy.z());

    auto n2_copy{n2};
    EXPECT_EQ(n2.x(), n2_copy.x());
    EXPECT_EQ(n2.y(), n2_copy.y());
    EXPECT_EQ(n2.z(), n2_copy.z());
}