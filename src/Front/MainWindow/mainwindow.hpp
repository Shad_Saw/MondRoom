#pragma once
#include <QMainWindow>
#include <QPushButton>
#include "../CreationWindow/creationwindow.hpp"
#include "../Form/form.hpp"

QT_BEGIN_NAMESPACE
namespace Ui
{
    class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow() override;

private slots:
    void on_NewProjetButton_clicked();
    void on_BackHomeButton2_clicked();
    void on_BackHomeButton_clicked();
    void on_HelpButton_clicked();
    void on_QuitButton_clicked();
    void on_AboutButton_clicked();
    void on_ImportBot_clicked();

private:
    Ui::MainWindow* ui;
};
