file(GLOB_RECURSE SOURCES LIST_DIRECTORIES true *.hpp *.cpp)
file(GLOB_RECURSE UI_SOURCES LIST_DIRECTORIES true *.ui)
set(LIB_NAME Front_lib)

include_directories(${CMAKE_SOURCE_DIR}/src)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

# qt
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt5 COMPONENTS Widgets REQUIRED)

qt5_add_resources (RESOURCES Resources/Resources.qrc)

add_library(${LIB_NAME} ${SOURCES} ${UI_SOURCES} ${RESOURCES} CreationWindow/Fstl.cpp CreationWindow/Fstl.hpp)

target_link_libraries(${LIB_NAME} PRIVATE StlObject_lib)
target_link_libraries(${LIB_NAME} PRIVATE StlParser_lib)

target_link_libraries(${LIB_NAME} PUBLIC Qt5::Widgets)