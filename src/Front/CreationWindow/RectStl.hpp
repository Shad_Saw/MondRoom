#pragma once
#include "../../STL/stl_object/ObjectBox.hpp"
#include <QGraphicsRectItem>


class RectStl : public QGraphicsRectItem
{
private:
    ObjectBox stl_box;
    int id;

public:
    enum { Type = UserType + 1 };

public:
    /// ID
    static int counter;

    /// c-tor
    RectStl(const ObjectStl& stl_object, const bool& middle);

    /// d-tor
    virtual ~RectStl();

    /// translate rectangle in x
    /// @param tx value to translate
    void translate_x(float tx);

    /// translate rectangle in y
    /// @param ty value to translate
    void translate_y(float ty);

    /// rotate object
    /// @param angle Angle to rotate in degree
    void rotate(float angle);

    /// scale object
    /// @param factor Factor to scale it
    void scale(float factor);

    /// change color of objet
    /// @param c color
    void set_color(QColor c);

    /// create stl object with transformation apply on
    ObjectStl get_stl_object() const;

    /// to get type of this item
    int type() const override;

    /// to get ID of this item
    int get_id() const;

    /// make a file of this rectangle
    /// @param path_dir path of directory
    void to_file(std::string path_dir) const;

protected:
    /// update position stl object in release
    /// @param event
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

    // static méthod
    /// static methode to create point QtPoint with Point3 x and y
    /// @param point Point to convert
    /// @return QtPoint
    static QPoint get_2d(const Point3& point);
};


