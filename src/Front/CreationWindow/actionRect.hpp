#pragma once
#include <functional>
#include "RectStl.hpp"

/// create function of translation in x to apply in rect
/// @param tx Factor of translation in x
/// @param inverted To inverted sens
/// @return function to apply
std::function<void(RectStl&)> get_func_translate_x(float tx,
                                                   const bool& inverted = false);

/// create function of translation in y to apply in rect
/// @param tx Factor of translation in y
/// @param inverted To inverted sens
/// @return function to apply
std::function<void(RectStl&)> get_func_translate_y(float ty,
                                                   const bool& inverted = false);


/// create function of rotation to apply in rect
/// @param angle Angle to ratate in degree
/// @param inverted To inverted sens
/// @return function to apply
std::function<void(RectStl&)> get_func_rotate(float angle,
                                              const bool& inverted = false);

/// create function of scale to apply in rect
/// @param factor Scale factor
/// @param inverted To inverted sens
/// @return function to apply
std::function<void(RectStl&)> get_func_scale(float factor,
                                             const bool& inverted = false);

/// create function of set color to apply in rect
/// @param c color c
/// @return function to apply
std::function<void(RectStl&)> get_func_set_color(QColor c);

