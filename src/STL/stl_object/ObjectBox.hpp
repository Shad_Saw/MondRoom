#pragma once
#include <eigen3/Eigen/Geometry>
#include "ObjectStl.hpp"

using Box = Eigen::AlignedBox<float, 3>;

class ObjectBox
{
private:
    Box box{};
    mutable ObjectStl objectstl;

    void reset_box();

public:
    /// c-tor
    ObjectBox() = delete;

    /// c-tor
    ObjectBox(const ObjectStl& stl, bool center=false);

    /// d-tor
    ~ObjectBox();

    /// max point
    /// @return Point3
    Point3 get_max() const;

    /// min point
    /// @return Point3
    Point3 get_min() const;

    /// point in center of box
    /// @return Point3
    Point3 get_center() const;

    /// volume of box
    /// @return float represent volume
    float get_volume() const;

    /// get stlobject in box
    /// @return StlObject
    const ObjectStl& get_stl_object() const;

    /// rotate in x
    /// @param angle Angle to rotate
    /// @param deg (default false) if true in degree, false in radian
    /// @return can be chained
    ObjectBox& rotate_x(float angle, bool deg = false);

    /// rotate in y
    /// @param angle Angle to rotate
    /// @param deg (default false) if true in degree, false in radian
    /// @return can be chained
    ObjectBox& rotate_y(float angle, bool deg = false);


    /// rotate in z
    /// @param angle Angle to rotate
    /// @param deg (default false) if true in degree, false in radian
    /// @return can be chained
    ObjectBox& rotate_z(float angle, bool deg = false);


    /// rotate with an axis
    /// @param axe Axis to rotate
    /// @param angle Angle to rotate
    /// @param deg (default false) if true in degree, false in radian
    /// @return can be chained
    ObjectBox& rotate(const Vect3& axe, float angle, bool deg = false);

    /// make a translation
    /// @param tx Tranlation in x
    /// @param ty Tranlation in y
    /// @param tz Tranlation in z
    /// @return can be chained
    ObjectBox& translate(float tx, float ty, float tz);

    /// translation with vector
    /// @param direction direction vector
    /// @return can be chained
    ObjectBox& translate(const Vect3& direction);

    /// cahnge scale of box
    /// @param size Coeficient to multiply
    /// @return can be chained
    ObjectBox& scale(float size);
};