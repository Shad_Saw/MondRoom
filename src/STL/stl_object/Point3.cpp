#include "Point3.hpp"

Point3::Point3()
    : Point3{0, 0, 0}
{}

Point3::Point3(float x, float y, float z)
: vec{x, y , z}
{}

Point3::Point3(const std::array<float, 3>& f)
    : Point3{f[0], f[1], f[2]}
{}

Point3::Point3(const Vect3& vect)
: vec{vect}
{}

float Point3::x() const
{
    return vec.x();
}

float Point3::y() const
{
    return vec.y();
}

float Point3::z() const
{
    return vec.z();
}

Point3& Point3::transform(const Transform& tranformation)
{
    vec = tranformation * vec;
    return *this;
}

std::ostream& operator<<(std::ostream& stream, const Point3& p3)
{
    return stream << p3.x() << " " << p3.y() << " " << p3.z();
}

Point3::operator PointEigen() const
{
    return vec;
}

bool Point3::operator==(const Point3& other) const
{
    return vec == other.vec;
}

bool Point3::operator!=(const Point3& other) const
{
    return !(*this == other);
}


