#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <memory>
#include "../Parser.hpp"
namespace stl::ascii
{
    /// Forward declarations of classes
    class Parser;
    class Scanner;
    class location;

    class Driver
    {
    private:
        int error = 0;
        std::string err_message;
        std::unique_ptr<Scanner> scanner;
        std::unique_ptr<Parser> parser;
        location* loc;

        std::shared_ptr<ObjectStl> object = nullptr;

        void err(const std::string&);
        void make_object(const std::string& name,
                         const std::vector<Triangle>& triangles);

        // tok make parsing
        void reset();
    public:
        Driver();
        ~Driver();

        std::shared_ptr<ObjectStl> parse();
        std::shared_ptr<ObjectStl> parse_file(const std::string& path);
        /// Allows Parser and Scanner to access private attributes
        /// of the Driver class
        friend class Parser;
        friend class Scanner;
    };
}
