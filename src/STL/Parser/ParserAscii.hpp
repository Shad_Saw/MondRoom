#pragma once
#include "Parser.hpp"

namespace stl
{
    class ParserAscii: public Parser
    {
    public:
        ParserAscii() = default;
        ~ParserAscii() = default;
        virtual std::shared_ptr<ObjectStl>
                operator()(const std::string& path) const;
    };
}