#pragma once
#include <cstdint>
#include <string>
#include <fstream>
#include <memory>
#include "../../stl_object/ObjectStl.hpp"
#include "ExceptionAscii.hpp"

namespace stl::binary
{
    class Parser
    {
        std::unique_ptr<std::ifstream> file;

    private:
        template<typename T>
        void read(T* var, unsigned int size = 1);

        std::shared_ptr<ObjectStl> parse();
        std::string get_header();
        uint32_t get_triangle_count();

        Triangle get_triangle();
        Vect3Normal get_normal();
        Point3 get_vertex();
        uint16_t get_attribut();

    public:
        Parser() = default;
        virtual ~Parser() = default;

        std::shared_ptr<ObjectStl> parse_file(const std::string& path);
    };
}

#include "Template_parser.hxx"
