#include "ExceptionAscii.hpp"

namespace stl::binary
{
    ExceptionAscii::ExceptionAscii()
            : std::exception()
    {}

    ExceptionAscii::~ExceptionAscii()
    {}

    const char* ExceptionAscii::what() const noexcept
    {
        return message.c_str();
    }
}