#include "Parser.hpp"
#include <stdexcept>
#include <algorithm>

namespace stl::binary
{
    std::shared_ptr<ObjectStl> Parser::parse_file(const std::string& path)
    {
        file = std::make_unique<std::ifstream>(path.c_str(), std::ios::binary);
        if (!file->is_open())
            throw std::runtime_error("cannot open file: " + path);

        return parse();
    }

    std::shared_ptr<ObjectStl> Parser::parse()
    {
        std::string name = get_header();

        uint32_t triangle_count = get_triangle_count();
        std::vector<Triangle> triangles;
        for (uint32_t i = 0; i < triangle_count; i++)
            triangles.emplace_back(get_triangle());

        return std::make_shared<ObjectStl>(name, triangles);
    }

    std::string Parser::get_header()
    {
        char header_char[ObjectStl::HEADER_SIZE];
        read(header_char, ObjectStl::HEADER_SIZE);

        std::string header(header_char, ObjectStl::HEADER_SIZE);

        if (header.find("solid") == 0)
            throw ExceptionAscii();

        header.erase(std::remove(header.begin(), header.end(), '\0'),
                     header.end());
        return header;
    }

    uint32_t Parser::get_triangle_count()
    {
        uint32_t count = 0;
        read(&count);
        return count;
    }

    Vect3Normal Parser::get_normal()
    {
        float n[3];
        read(n, 3);
        return Vect3Normal{n[0], n[1], n[2]};
    }

    Point3 Parser::get_vertex()
    {
        float n[3];
        read(n, 3);
        return Point3{n[0], n[1], n[2]};
    }

    Triangle Parser::get_triangle()
    {
        Vect3Normal normal = get_normal();

        std::array<Point3, 3> arr_vertex;
        for (int i = 0; i < 3; i++)
            arr_vertex[i] = get_vertex();

        uint16_t attr = get_attribut();

        return Triangle(normal, arr_vertex, attr);
    }

    uint16_t Parser::get_attribut()
    {
        uint16_t attr;
        read(&attr);
        return attr;
    }


}
//file->read((char*)var, size * sizeof(var) / sizeof(char));
